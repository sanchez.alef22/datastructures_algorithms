#include <algorithm>
#include <iostream>
#include <vector>
#include <limits>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

using std::cin;
using std::cout;
using std::endl;
using std::ios_base;
using std::vector;

// rule for it to be a correct BST is 
// for any node of the tree, if it's key is x, then for any node in its left subtree and its ket must be..
// .. strictly less than x, and for any node in its right sybtree its key must be strictly greater than x
// in other words: smaller elements are to the left and bigger elements are to the right 
// you need to check whether the given binary tree structure satisfies this condition.
// you are guaranteed that the input contains a valid binary tree- that is it is a tree and each node contains..
// .. at most two children.


/*struct Node {
  int key;
  int left;
  int right;

  Node() : key(0), left(-1), right(-1) {}
  Node(int key_, int left_, int right_) : key(key_), left(left_), right(right_) {}
};
*/

class Node
{
public:
    int key;
    Node* left;
    Node* right;
    Node(int data)
    {
        key = data;
        left = nullptr;
        right = nullptr;
    }
};

class TreeOrders
{
public:
    int n;
    vector <int> key;
    vector <int> left;
    vector <int> right;
    Node* root;
    int root_val;
    int current_max;
    void read()
    {
        cin >> n;
        if (n == 0)
        {
            root = nullptr;
        }
        else
        {
            key.resize(n);
            left.resize(n);
            right.resize(n);
            for (int i = 0; i < n; i++)
            {
                cin >> key[i] >> left[i] >> right[i];
            }
            root = new Node(key[0]);
            Tree_builder(root, 0);
        }
    }

    void Tree_builder(Node* root, int i)
    {
        if (left[i] != -1)
        {
            Node* temp = new Node(key[left[i]]);
            root->left = temp;
            Tree_builder(root->left, left[i]);
        }
        if (right[i] != -1)
        {
            Node* temp = new Node(key[right[i]]);
            root->right = temp;
            Tree_builder(root->right, right[i]);
        }
    }
    
    bool is_bst(void)
    {
        return recursive_bst(root, nullptr, nullptr);
    }
    bool recursive_bst(Node* root, Node * min, Node* max)
    {
        if (root != nullptr)
        {
            if ((max != nullptr && root->key >= max->key) || (min != nullptr && root->key < min->key))
            {
                return false;
            }
        }
        else
        {
            return true;
        }
        return( (recursive_bst(root->left, min, root)) && (recursive_bst(root->right, root, max)));
    }
    bool in_Order(Node* root, Node* previous)
    {
        if(root != nullptr)
        {
          if(previous !=  nullptr && root->key > previous->key)
          {
            return false;
          }
          if(previous == nullptr)
          {
            previous = root;
          }
        }
        else
        {
          return true;
        }
        return in_Order(root->left, root) && in_Order(root, root->right);
    }
    bool in_order(void)
    {
      return in_Order(root, nullptr);
    }
};

int main_with_large_stack_space()
{
    ios_base::sync_with_stdio(0);
    bool result = false;
    int max;
    TreeOrders t;
    t.read();
    //result = t.is_bst();
    result = t.in_order();
    if (result == true)
    {
        cout << "CORRECT" << endl;
    }
    else
    {
        cout << "INCORRECT" << endl;
    }
    return 0;
}
int main(int argc, char** argv)
{
#if defined(__unix__) || defined(__APPLE__)
    // Allow larger stack space
    const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                std::cerr << "setrlimit returned result = " << result << std::endl;
            }
        }
    }
#endif

    return main_with_large_stack_space();
}
