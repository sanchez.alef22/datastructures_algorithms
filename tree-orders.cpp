#include <iostream>
#include <vector>
#include <algorithm>
#if defined(__unix__) || defined(__APPLE__)
#include <sys/resource.h>
#endif

using std::vector;
using std::ios_base;
using std::cin;
using std::cout;

class Node
{
public:
    int key;
    Node *left;
    Node *right;
    Node(int data)
    {
        key = data;
        left = nullptr;
        right = nullptr;
    }
};
class TreeOrders
{
    int n;
    vector <int> key;
    vector <int> left;
    vector <int> right;
    Node* root;

public:
    void read() {
        cin >> n;
        key.resize(n);
        left.resize(n);
        right.resize(n);

      for (int i = 0; i < n; i++)
      {
         cin >> key[i] >> left[i] >> right[i];
      }
      root = new Node(key[0]);
      Tree_builder(root, 0);
    }

    void Tree_builder(Node* root, int i )
    {
        if (left[i] != -1)
        {
            Node* temp = new Node(key[left[i]]);
            root->left = temp;
            Tree_builder(root->left, left[i]);
        }
        if (right[i] != -1)
        {
            Node* temp = new Node(key[right[i]]);
            root->right = temp;
            Tree_builder(root->right, right[i]);
        }
    }
    void in_order()
    {
        in_Order(root);
    }
    void in_Order(Node* root)
    {
        if (root !=nullptr)
        {
            in_Order(root->left);
            std::cout<< root->key<<" ";
            in_Order(root->right);
        }
    }
    void Pre_Order(Node* root)
    {
        if (root != nullptr)
        {
            std::cout << root->key << " ";
            Pre_Order(root->left);
            Pre_Order(root->right);
        }
    }
    void Post_Order(Node* root)
    {
        if (root != nullptr)
        {
            Post_Order(root->left);
            Post_Order(root->right);
            std::cout << root->key << " ";
        }
    }

    void pre_order() {

        Pre_Order(root);

    }


    void post_order(){

        Post_Order(root);

    }
};


void print(vector <int> a) {
    for (size_t i = 0; i < a.size(); i++) {
        if (i > 0) {
            cout << ' ';
        }
        cout << a[i];
    }
    cout << '\n';
}

int main_with_large_stack_space() {
    ios_base::sync_with_stdio(0);
    TreeOrders t;
    t.read();
    //print(t.in_order());
    //print(t.pre_order());
    //print(t.post_order());
    t.in_order();
    cout << '\n';
    t.pre_order();
    cout << '\n';
    t.post_order();
    return 0;
}

int main(int argc, char** argv)
{
#if defined(__unix__) || defined(__APPLE__)
    // Allow larger stack space
    const rlim_t kStackSize = 16 * 1024 * 1024;   // min stack size = 16 MB
    struct rlimit rl;
    int result;

    result = getrlimit(RLIMIT_STACK, &rl);
    if (result == 0)
    {
        if (rl.rlim_cur < kStackSize)
        {
            rl.rlim_cur = kStackSize;
            result = setrlimit(RLIMIT_STACK, &rl);
            if (result != 0)
            {
                std::cerr << "setrlimit returned result = " << result << std::endl;
            }
        }
    }
#endif

    return main_with_large_stack_space();
}
